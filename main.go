package main

import (
	"fmt"

	"bitbucket.org/billing/go-regenerate/app"
	"bitbucket.org/billing/go-regenerate/handler"
	"bitbucket.org/billing/go-regenerate/model"
)

func main() {
	for {
		app.Initialize()

		flowControl()

		app.Sleeping()
	}
}

func flowControl() error {
	fmt.Printf("[ Getting Jobs From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	modelProcess := model.ProcessRegenerate{}

	for _, v := range listProcess {
		fmt.Println("[ Processing Data Tapin Only ] : { Process ID :", v.ProcessID, ", Batch ID :", v.BatchID, ", Original Tapin Only Total Record :", v.OriTapinOnlyCount, "}")
		modelProcess.UpdateStatus(v.ProcessID, "4")

		handlerTAPINOnly := handler.TAPINOnly{ListProcess: &v}

		handlerTAPINOnly.RegenerateData()

		handlerProcess.CloseProcess(&v, handlerTAPINOnly.ProcessStatusID, handlerTAPINOnly.ErrorMessage, handlerTAPINOnly.TotalNewTapinOnly)
	}

	return nil
}
