package model

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"bitbucket.org/billing/go-regenerate/app"
)

// TAPINOnly : Struct of TAPINOnly Model
type TAPINOnly struct {
	BatchID   string `json:"batch_id"`
	TableName string `json:"table_name"`
}

// GetDataTapinOnly :
func (p *TAPINOnly) GetDataTapinOnly() (StructDataRegenerate, error) {
	var output StructDataRegenerate

	response, err := http.Get(app.Appl.DBAPIURL + "process/regenerate/getData/" + p.TableName + "/" + p.BatchID)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return StructDataRegenerate{}, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}
