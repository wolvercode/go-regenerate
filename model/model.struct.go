package model

// ProcessRegenerate :
type ProcessRegenerate struct {
	ProcessID         string `json:"process_id"`
	ProcesstypeID     string `json:"processtype_id"`
	BatchID           string `json:"batch_id"`
	DataTypeID        int    `json:"data_type_id"`
	Periode           string `json:"periode"`
	DayPeriode        string `json:"day"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	OnlyID            int    `json:"only_id"`
	PathOnly          string `json:"path_only"`
	FilenameOnly      string `json:"filename_only"`
	MatchID           int    `json:"match_id"`
	PathMatch         string `json:"path_match"`
	FilenameMatch     string `json:"filename_match"`
	MaxDelay          string `json:"max_delay"`
	TableName         string `json:"table_name"`
	OriTapinOnlyCount int    `json:"ori_tapin_only_count"`
}

// StructDataRegenerate :
type StructDataRegenerate struct {
	CountData      int      `json:"count_data"`
	DataRegenerate []string `json:"data_regenerate"`
}

// ClosingStruct :
type ClosingStruct struct {
	StructProcess     *ProcessRegenerate `json:"process_conf"`
	ProcessStatusID   string             `json:"process_status_id"`
	TotalNewTapinOnly int                `json:"total_new_tapin_only"`
	ErrorMessage      string             `json:"error_message"`
}
