package handler

import (
	"fmt"

	"bitbucket.org/billing/go-regenerate/model"
)

// Process :
type Process struct {
	ProcessStatusID string `json:"processstatus_id"`
}

var modelProcess model.ProcessRegenerate

// GetProcess :
func (p *Process) GetProcess() ([]model.ProcessRegenerate, error) {
	listProcess, err := modelProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}

// CloseProcess :
func (p *Process) CloseProcess(process *model.ProcessRegenerate, processStatusID, errorMessage string, totalNewTapin int) error {
	err := modelProcess.CloseProcess(process, processStatusID, errorMessage, totalNewTapin)
	if err != nil {
		return err
	}
	return nil
}
