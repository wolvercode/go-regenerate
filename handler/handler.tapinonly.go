package handler

import (
	"bufio"
	"fmt"
	"os"

	"bitbucket.org/billing/go-regenerate/model"
)

// TAPINOnly :
type TAPINOnly struct {
	ListProcess       *model.ProcessRegenerate
	ProcessStatusID   string `json:"process_status_id"`
	TotalNewTapinOnly int    `json:"total_new_tapin_only"`
	ErrorMessage      string `json:"error_message"`
}

// RegenerateData :
func (p *TAPINOnly) RegenerateData() error {

	fmt.Printf("  > [ Get Tapin Only Data From Database ] : ")
	modelTAPINOnly := model.TAPINOnly{BatchID: p.ListProcess.BatchID, TableName: p.ListProcess.TableName}

	dataTapin, err := modelTAPINOnly.GetDataTapinOnly()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.ProcessStatusID, p.ErrorMessage = "5", err.Error()
		return err
	}
	if dataTapin.CountData == 0 {
		fmt.Println("Failed, no data found/no data with flag to file in database")
		p.ProcessStatusID, p.ErrorMessage = "5", "No Data with BatchID "+p.ListProcess.BatchID+" Found in Table "+p.ListProcess.TableName
		return nil
	}
	fmt.Println("Success")

	if dataTapin.CountData == p.ListProcess.OriTapinOnlyCount {
		fmt.Println("  > [ Processing Success ] : { No Change To Data, Total Record :", p.ListProcess.OriTapinOnlyCount, "}")

		p.ProcessStatusID, p.TotalNewTapinOnly, p.ErrorMessage = "7", p.ListProcess.OriTapinOnlyCount, ""

		return nil
	}

	fmt.Println("  > [ Writing Data To New TAPIN Only ]")
	inputOnly := p.ListProcess.PathOnly + p.ListProcess.FilenameOnly
	fmt.Println("    > [ TAPIN Only File ] : " + inputOnly)
	oriTapinOnly := p.ListProcess.PathOnly + p.ListProcess.FilenameOnly + ".ori"
	_, err = os.Stat(oriTapinOnly)
	if os.IsNotExist(err) {
		fmt.Println("    > [ Renaming Original TAPIN Only File To ] : " + oriTapinOnly)
		os.Rename(inputOnly, oriTapinOnly)
	}

	fileOnly, err := os.Create(inputOnly)
	if err != nil {
		fmt.Println("  > [ Creating File Only ] : Failed, with error =>", err.Error())
		p.ProcessStatusID, p.ErrorMessage = "5", err.Error()
		return err
	}

	writerOnly := bufio.NewWriter(fileOnly)

	countOut := 0
	for _, line := range dataTapin.DataRegenerate {
		fmt.Fprintln(writerOnly, line)
		countOut++
	}

	// File only bufio writer flush
	err = writerOnly.Flush()
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		p.ProcessStatusID, p.ErrorMessage = "5", err.Error()
		return err
	}
	fileOnly.Close()

	fmt.Println("  > [ Processing Success ] : { Total New Tapin Only Record :", countOut, "}")

	p.ProcessStatusID, p.TotalNewTapinOnly, p.ErrorMessage = "7", countOut, ""

	return nil
}
